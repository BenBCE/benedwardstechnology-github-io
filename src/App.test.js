import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App,  {Header,
Footer,
PostPreview,
P_home,
P_about,
P_contact,
P_route} from './App';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

describe('P_route', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<P_route />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <P_route />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('P_contact', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<P_contact />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <P_contact />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('P_about', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<P_about />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <P_about />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('P_home', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<P_home />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <P_home />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('PostPreview', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PostPreview />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <PostPreview />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('Footer', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Footer />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <Footer />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('Header', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Header />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <Header />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('App', () =>{
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot',()=>{
    const component = renderer.create(
      <App />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

});
