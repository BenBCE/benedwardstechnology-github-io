import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './css/clean-blog.min.css';
import 'jquery';
import 'bootstrap';
import {Datetime} from 'react-datetime';


/* Get images dynamically
*
* doens't work with JEST tests */
// function importAll(r) {
//   let images = {};
//   r.keys().map((item, index) => {
//     images[item.replace('./', '')] = r(item);
//   });
//   return images;
// }
//
// const images = importAll(require.context('./img', false, /\.(png|jpe?g|svg)$/));

import bgImg from './img/home-bg.jpg';

const blog = [
  {
    id: 1,
    title: "ben 1",
    content: "ben content"
  }, {
    id: 2,
    title: "ben 1",
    content: "ben content"
  }
];

const Navigation = () => <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
  <div className="container">
    <a className="navbar-brand" href="index.html">Edwards</a>
    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i className="fa fa-bars"></i>
    </button>
    <div className="collapse navbar-collapse" id="navbarResponsive">
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/">Home</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/About">About</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/Contact">Contact</Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/Clock">Clock</Link>
        </li>
      </ul>
    </div>
  </div>
</nav>

const Header = (props) => <header className="masthead" style={{
  backgroundImage: `url('${props.img}')`
}}>
  <div className="overlay"></div>
  <div className="container">
    <div className="row">
      <div className="col-lg-8 col-md-10 mx-auto">
        {props.img && <div className="site-heading">
          <h1>{props.title}</h1>
          <span className="subheading">{props.subtitle}</span>
        </div>}
      </div>
    </div>
  </div>
</header>

const Footer = () => <footer>
  {/*<div className="container">
    <div className="row">
      <div className="col-lg-8 col-md-10 mx-auto">
        <ul className="list-inline text-center">
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-facebook fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#">
              <span className="fa-stack fa-lg">
                <i className="fa fa-circle fa-stack-2x"></i>
                <i className="fa fa-github fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
        </ul>
        <p className="copyright text-muted">Copyright &copy; Your Website 2018</p>
      </div>
    </div>
  </div>
  */}
</footer>

const PostPreview = (props) => <div className="post-preview">
  <a onClick={props.onClick}>
    <h2 className="post-title">
      {props.title}
    </h2>
    <h3 className="post-subtitle">
      {props.subtitle}
    </h3>
  </a>
  <p className="post-meta">Posted by {props.writter}
    on {props.date}</p>
  <hr/>
</div>

class CountDown extends Component {
  constructor(props) {
    super(props);

    let now = new Date();
    this.state = {
      untill: new Date(now.getFullYear(), now.getMonth(), now.getDay(), 17, 0),
      diff: null,
      test: 'help'
    };

    this.startIntervalClock = this.startIntervalClock.bind(this);
    this.setClock = this.setClock.bind(this);
    this.intervalFunction = this.intervalFunction.bind(this);
    this.clearTimer = this.clearTimer.bind(this);
  }

  componentDidMount() {
    if (localStorage.getItem('untill') !== 'null') {
      console.log('got untill from localstorage');
      this.setState({untill: localStorage.getItem('untill')});
      this.startIntervalClock();
    }
  }

  leadingZero(value) {
    if (value < 10) {
      return '0' + value;
    } else {
      return value;
    }
  }

  startIntervalClock() {
    this.interval = setInterval(() => this.intervalFunction(), 1000);

  }

  intervalFunction() {

    let difference = new Date(this.state.untill) - new Date();
    let differenceTime = new Date(difference);
    localStorage.setItem('untill', this.state.untill);
    if ((differenceTime.getHours() - 1) < 0) {
      this.setState({diff: 'Go Home'});
      return false;
    }

    let respTime = this.leadingZero(differenceTime.getHours() - 1) + ' : ' + this.leadingZero(differenceTime.getMinutes()) + ' : ' + this.leadingZero(differenceTime.getSeconds());
    this.setState({diff: respTime})
  }

  setClock(evt) {
    this.setState({untill: evt.target.value})
  }

  componentWillUnmount() {
    this.closeWindow();
  }

  closeWindow() {
    let now = new Date();
    clearInterval(this.interval);
    this.setState({
      diff: null,
      untill: new Date(now.getFullYear(), now.getMonth(), now.getDay(), 17, 0)
    });
  }

  clearTimer() {
    this.closeWindow();
    localStorage.setItem('untill', null);
  }

  render() {
    const {untill} = this.state;
    return (

      <div className="App">
        {!this.state.diff && <div>
          <input size="100" type="text" value={this.state.untill} id="clock" onChange={this.setClock}/>
          <button type="submit" onClick={this.startIntervalClock}>Submit</button>
        </div>
}
        {this.state.diff && <div>
          <h1>{this.state.diff}</h1>
          <br/> {untill && <h3>{this.state.untill.toString()}</h3>}
          <br/> {untill && <h3>{localStorage.getItem('untill')}</h3>}

          <button type="button" onClick={this.clearTimer}>Clear</button>
        </div>
}
      </div>
    );
  }
}

class P_home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      blog: null,
      article: 3
    }
    this.addBlog = this.addBlog.bind(this);
  }

  addBlog() {
    console.log(this.state.blog);
    this.setState({
      blog: [
        ...this.state.blog, {
          id: this.state.article,
          title: "article " + this.state.article,
          content: "test"
        }
      ],
      article: this.state.article + 1
    });
  }

  componentDidMount() {
    this.setState({blog})
  }

  render() {
    const {blog} = this.state;
    return <div>
    <Datetime />
    </div>

  }
}

const P_about = () => <div>About</div>

const P_contact = () => <div>Contact</div>

// export default App;
const P_route = () => (
  <Router>
    <div>
      <Navigation/>
      <Route exact path="/" component={P_home}/>
      <Route path="/About" component={P_about}/>
      <Route path="/Contact" component={P_contact}/>
      <Route path="/Clock" component={P_clock}/>
      <Footer/>
    </div>
  </Router>
)

const P_clock = () => <div>
  <Header title="Clock" subtitle="test"/>
  <div className="container">
    <div className="row">
      <div className="col-lg-8 col-md-10 mx-auto">
        <CountDown/>
      </div>
    </div>
  </div>
</div>

export default P_route

export {
  Header,
  Footer,
  PostPreview,
  P_home,
  P_about,
  P_contact,
  P_route
}
