var gulp = require('gulp');
var bump = require('gulp-bump');
var log = require('gulplog');
var git = require('gulp-git');
var fs = require('fs');
var run = require('gulp-run-command').default;
var clean = require('gulp-clean');

gulp.task('delete-files-static',function(){
  return gulp.src('static', {read: false, allowEmpty: true})
          .pipe(clean());
})

gulp.task('delete-files-index',function(){
  return gulp.src('index.html', {read: false, allowEmpty: true})
          .pipe(clean());
})

gulp.task('delete-files-build',function(){
  return gulp.src('build', {read: false, allowEmpty: true})
          .pipe(clean());
})

gulp.task('delete-files',gulp.series(
  'delete-files-static',
  'delete-files-index',
  'delete-files-build',
))


gulp.task('bump-version', function () {
// We hardcode the version change type to 'patch' but it may be a good idea to
// use minimist (https://www.npmjs.com/package/minimist) to determine with a
// command argument whether you are doing a 'major', 'minor' or a 'patch' change.
  return gulp.src(['./package.json'])
    .pipe(bump({type: "patch"}).on('error', log.error))
    .pipe(gulp.dest('./'));
});

gulp.task('commit-changes', function () {
  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit(process.env.COMMIT || 'Script update'));
});

gulp.task('push-changes', function (done) {
  git.push('origin', 'master', done);
});

gulp.task('create-new-tag', function (done) {
  var version = getPackageJsonVersion();
  git.tag(version, 'Created Tag for version: ' + version, function (error) {
    if (error) {
      return done(error);
    }
    git.push('origin', 'master', {args: '--tags'}, done);
  });

  function getPackageJsonVersion () {
    // We parse the json file instead of using require because require caches
    // multiple calls so the version number won't be updated
    return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
  };
});
gulp.task('build-react', run('npm run build'));

gulp.task('copy-build', function(done){
  return gulp.src(['build/**/*']).pipe(gulp.dest('.'));
})

gulp.task('release', gulp.series(
  'delete-files',
  'build-react',
  'copy-build',
  'bump-version',
  'commit-changes',
  'push-changes',
  'create-new-tag',
));
